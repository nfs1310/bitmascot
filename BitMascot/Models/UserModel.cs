﻿using BitMascot.Enum;
using BitMascot.HelperCodes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BitMascot.Models
{
    public class UserModel
    {
        public int UserID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public string PhoneNo { get; set; }
        public string Email { get; set; }
        public DateTime DOB { get; set; }
        public string Password { get; set; }
        public UserType UserType { get; set; }
        public bool RememberMe { get; set; }
        public string NewPassword { get; set; }

        DataAccess.DataAccess DA;

        public AnyStatus RegisterUser()
        {
            UserModel NewUser = new UserModel()
            {
                FirstName = this.FirstName,
                LastName = this.LastName,
                Address = this.Address,
                PhoneNo = this.PhoneNo,
                Email = this.Email,
                DOB = this.DOB,
                Password = PasswordHashing.HashPassword(this.Password),
                UserType = UserType.User
            };

            DA = new DataAccess.DataAccess();
            return (AnyStatus)DA.AddNewUser(NewUser);
        }

        public UserModel UserLogin()
        {
            DA = new DataAccess.DataAccess();
            return DA.GetUserByEmail(this.Email);
        }

        public AnyStatus ChangePassword()
        {
            DA = new DataAccess.DataAccess();
            UserModel UserByID = DA.GetUserByID(this.UserID);

            if (PasswordHashing.VerifyPassWord(this.Password, UserByID.Password))
            {
                return (AnyStatus)DA.ChangeUserPassword(this.UserID, PasswordHashing.HashPassword(this.NewPassword));
            }
            else
            {
                return AnyStatus.Failed;
            }
        }

        public UserModel GetUserByID(int UserID)
        {
            DA = new DataAccess.DataAccess();
            return DA.GetUserByID(UserID);
        }

        public List<UserModel> GetUserList()
        {
            DA = new DataAccess.DataAccess();
            return DA.GetUserList();
        }

    }
}