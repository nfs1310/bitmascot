﻿using BitMascot.Enum;
using BitMascot.HelperCodes;
using BitMascot.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace BitMascot.Controllers
{
    public class AnonymousController : Controller
    {
        // GET: Anonymous
        public ActionResult Login()
        {
            return View();
        }

        public ActionResult Register()
        {
            return View();
        }

        public JsonResult RegisterNewUser(UserModel NewUser)
        {
            object data = null;

            data = NewUser.RegisterUser();

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult UserLogin(UserModel LoginInfo)
        {
            object data = null;

            //data = NewUser.RegisterUser();
            UserModel UserByEmail = LoginInfo.UserLogin();

            if (UserByEmail != null)
            {
                if (PasswordHashing.VerifyPassWord(LoginInfo.Password, UserByEmail.Password))
                {
                    Session["UserModel"] = UserByEmail;
                    FormsAuthentication.SetAuthCookie(UserByEmail.Email, LoginInfo.RememberMe);

                    data = AnyStatus.Successful;
                }
                else
                {
                    data = AnyStatus.Failed;
                }
            }
            else
            {
                data = AnyStatus.Failed;
            }

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            Session["UserModel"] = null;
            return RedirectToAction("Login");
        }

    }
}