﻿using BitMascot.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BitMascot.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult ChangePassword()
        {
            return View();
        }

        public JsonResult ChangeUserPassword(UserModel ChangeUserPassword)
        {
            object data = null;
            
            data = ChangeUserPassword.ChangePassword();

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult UserProfile()
        {
            return View();
        }

        public JsonResult GetUserByID(int UserID)
        {
            object data = null;

            UserModel UserInfo = new UserModel();
            data = UserInfo.GetUserByID(UserID);

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult UserList()
        {
            return View();
        }

        public JsonResult GetUserList()
        {
            object data = null;

            UserModel UserInfo = new UserModel();
            data = UserInfo.GetUserList();

            return Json(data, JsonRequestBehavior.AllowGet);
        }
    }
}