﻿using BitMascot.Enum;
using BitMascot.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace BitMascot.DataAccess
{
    public class DataAccess
    {
        private static string ConnectiontString = ConfigurationManager.ConnectionStrings["DBConnection"].ToString();

        public int AddNewUser(UserModel NewUser)
        {
            int Successful = 0;

            SqlConnection con = new SqlConnection(ConnectiontString);
            string query = "[dbo].[spUser_AddNewUser]";
            SqlCommand cmd = new SqlCommand(query, con)
            {
                CommandType = CommandType.StoredProcedure
            };

            cmd.Parameters.Add("@firstName", SqlDbType.NVarChar).Value = NewUser.FirstName;
            cmd.Parameters.Add("@lastName", SqlDbType.NVarChar).Value = NewUser.LastName;
            cmd.Parameters.Add("@address", SqlDbType.NVarChar).Value = NewUser.Address;
            cmd.Parameters.Add("@phoneNo", SqlDbType.NVarChar).Value = NewUser.PhoneNo;
            cmd.Parameters.Add("@email", SqlDbType.NVarChar).Value = NewUser.Email;
            cmd.Parameters.Add("@dob", SqlDbType.DateTime).Value = NewUser.DOB;
            cmd.Parameters.Add("@password", SqlDbType.NVarChar).Value = NewUser.Password;
            cmd.Parameters.Add("@userType", SqlDbType.Int).Value = (int)NewUser.UserType;

            SqlDataReader reader;

            try
            {
                con.Open();
                reader = cmd.ExecuteReader();

                if (reader.Read() && reader.HasRows)
                {
                    if (!DBNull.Value.Equals(reader["Successful"]))
                    {
                        Successful = Convert.ToInt32(reader["Successful"]);
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            reader.Close();
            con.Close();

            return Successful;
        }

        public UserModel GetUserByEmail(string Email)
        {
            UserModel UserInfo = null;

            SqlConnection con = new SqlConnection(ConnectiontString);
            string query = "[dbo].[spUser_GetUserByEmail]";
            SqlCommand cmd = new SqlCommand(query, con)
            {
                CommandType = CommandType.StoredProcedure
            };

            cmd.Parameters.Add("@email", SqlDbType.NVarChar).Value = Email;

            SqlDataReader reader;

            try
            {
                con.Open();
                reader = cmd.ExecuteReader();

                if (reader.Read() && reader.HasRows)
                {
                    UserInfo = new UserModel()
                    {
                        UserID = Convert.ToInt32(reader["UserID"]),
                        FirstName = reader["FirstName"].ToString(),
                        LastName = reader["LastName"].ToString(),
                        Address = reader["Address"].ToString(),
                        PhoneNo = reader["PhoneNo"].ToString(),
                        Email = reader["Email"].ToString(),
                        DOB = Convert.ToDateTime(reader["DOB"]),
                        UserType = (UserType)Convert.ToInt32(reader["UserType"]),
                        Password = reader["Password"].ToString()
                    };
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            reader.Close();
            con.Close();

            return UserInfo;
        }

        public UserModel GetUserByID(int ID)
        {
            UserModel UserInfo = null;

            SqlConnection con = new SqlConnection(ConnectiontString);
            string query = "[dbo].[spUser_GetUserByID]";
            SqlCommand cmd = new SqlCommand(query, con)
            {
                CommandType = CommandType.StoredProcedure
            };

            cmd.Parameters.Add("@id", SqlDbType.Int).Value = ID;

            SqlDataReader reader;

            try
            {
                con.Open();
                reader = cmd.ExecuteReader();

                if (reader.Read() && reader.HasRows)
                {
                    UserInfo = new UserModel()
                    {
                        UserID = Convert.ToInt32(reader["UserID"]),
                        FirstName = reader["FirstName"].ToString(),
                        LastName = reader["LastName"].ToString(),
                        Address = reader["Address"].ToString(),
                        PhoneNo = reader["PhoneNo"].ToString(),
                        Email = reader["Email"].ToString(),
                        DOB = Convert.ToDateTime(reader["DOB"]),
                        UserType = (UserType)Convert.ToInt32(reader["UserType"]),
                        Password = reader["Password"].ToString()
                    };
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            reader.Close();
            con.Close();

            return UserInfo;
        }

        public int ChangeUserPassword(int UserID, string NewPassword)
        {
            int Successful = 0;

            SqlConnection con = new SqlConnection(ConnectiontString);
            string query = "[dbo].[spUser_UpdateUserPassword]";
            SqlCommand cmd = new SqlCommand(query, con)
            {
                CommandType = CommandType.StoredProcedure
            };

            cmd.Parameters.Add("@id", SqlDbType.Int).Value = UserID;
            cmd.Parameters.Add("@password", SqlDbType.NVarChar).Value = NewPassword;

            SqlDataReader reader;

            try
            {
                con.Open();
                reader = cmd.ExecuteReader();

                if (reader.Read() && reader.HasRows)
                {
                    if (!DBNull.Value.Equals(reader["Successful"]))
                    {
                        Successful = Convert.ToInt32(reader["Successful"]);
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            reader.Close();
            con.Close();

            return Successful;
        }

        public List<UserModel> GetUserList()
        {
            List<UserModel> UserList = new List<UserModel>();

            SqlConnection con = new SqlConnection(ConnectiontString);
            string query = "[dbo].[spUser_GetUserList]";
            SqlCommand cmd = new SqlCommand(query, con)
            {
                CommandType = CommandType.StoredProcedure
            };
            
            SqlDataReader reader;

            try
            {
                con.Open();
                reader = cmd.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        UserModel UserInfo = new UserModel()
                        {
                            UserID = Convert.ToInt32(reader["UserID"]),
                            FirstName = reader["FirstName"].ToString(),
                            LastName = reader["LastName"].ToString(),
                            Address = reader["Address"].ToString(),
                            PhoneNo = reader["PhoneNo"].ToString(),
                            Email = reader["Email"].ToString(),
                            DOB = Convert.ToDateTime(reader["DOB"]),
                            UserType = (UserType)Convert.ToInt32(reader["UserType"]),
                            Password = reader["Password"].ToString()
                        };
                        UserList.Add(UserInfo);
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            reader.Close();
            con.Close();

            return UserList;
        }
    }
}