﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BCrypt.Net;

namespace BitMascot.HelperCodes
{
    public static class PasswordHashing
    {
        public static string HashPassword(string Password)
        {
            return BCrypt.Net.BCrypt.HashPassword(Password, BCrypt.Net.BCrypt.GenerateSalt(12));
        }

        public static bool VerifyPassWord(string Pasword, string HashText)
        {
            return BCrypt.Net.BCrypt.Verify(Pasword, HashText);
        }
    }
}