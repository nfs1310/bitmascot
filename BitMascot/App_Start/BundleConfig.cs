﻿using System.Web;
using System.Web.Optimization;

namespace BitMascot
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/allscripts").Include(
                        "~/Scripts/jquery-{version}.js",
                        "~/Scripts/jquery.min.js",
                        "~/Scripts/jQueryUI/jquery-ui.min.js",
                        "~/Scripts/bootstrap.min.js",
                        "~/Scripts/raphael-min.js",
                        "~/Scripts/morris/morris.min.js",
                        "~/Scripts/sparkline/jquery.sparkline.min.js",
                        "~/Scripts/jvectormap/jquery-jvectormap-1.2.2.min.js",
                        "~/Scripts/jvectormap/jquery-jvectormap-world-mill-en.js",
                        "~/Scripts/knob/jquery.knob.js",
                        "~/Scripts/moment.min.js",
                        "~/Scripts/daterangepicker/daterangepicker.js",
                        "~/Scripts/datepicker/bootstrap-datepicker.js",
                        "~/Scripts/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js",
                        "~/Scripts/slimScroll/jquery.slimscroll.min.js",
                        "~/Scripts/fastclick/fastclick.min.js",
                        "~/Scripts/app.min.js",
                        "~/Scripts/dashboard.js",
                        "~/Scripts/demo.js"));

            bundles.Add(new StyleBundle("~/Content/css/allstyles").Include(
                                         "~/Content/bootstrap.min.css",
                                        "~/Content/font-awesome.min.css",
                                        "~/Content/ionicons.min.css",
                                        "~/Content/AdminLTE.min.css",
                                        "~/Content/_all-skins.css",
                                        "~/Content/iCheck/flat/blue.css",
                                        "~/Content/morris.css",
                                        "~/Content/jvectormap/jquery-jvectormap-1.2.2.css",
                                       "~/Content/datepicker/datepicker3.css",
                                       "~/Content/daterangepicker/daterangepicker.css",
                                        "~/Content/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css"));
        }
    }
}
