﻿

function sendAjax(jsData, url, type, successMessage, str) {
    var returnData = "";
    $.ajax({
        async: false,
        type: type,
        url: url,
        data: str === "" ? JSON.stringify(jsData) : jsData, //JSON.stringify(jsData),
        contentType: "application/json;charset=utf-8",
        success: function (data) {
            console.log(data);
            returnData = data;
            if (successMessage !== "") {
                window.swal({
                    title: successMessage,
                    type: "success",
                    showCancelButton: false,
                    confirmButtonClass: 'btn-danger',
                    confirmButtonText: 'OK!'
                });
            }
        },
        error: function () {
            window.swal({
                title: "Error !",
                type: "error",
                showCancelButton: false,
                confirmButtonClass: 'btn-danger',
                confirmButtonText: 'Danger!'
            });
        }

    });
    return returnData;
}