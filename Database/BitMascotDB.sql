USE [master]
GO
/****** Object:  Database [BitMascot]    Script Date: 4/23/2019 12:10:09 PM ******/
CREATE DATABASE [BitMascot]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'BitMascot', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\BitMascot.mdf' , SIZE = 3072KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'BitMascot_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\BitMascot_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [BitMascot] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [BitMascot].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [BitMascot] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [BitMascot] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [BitMascot] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [BitMascot] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [BitMascot] SET ARITHABORT OFF 
GO
ALTER DATABASE [BitMascot] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [BitMascot] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [BitMascot] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [BitMascot] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [BitMascot] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [BitMascot] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [BitMascot] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [BitMascot] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [BitMascot] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [BitMascot] SET  DISABLE_BROKER 
GO
ALTER DATABASE [BitMascot] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [BitMascot] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [BitMascot] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [BitMascot] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [BitMascot] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [BitMascot] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [BitMascot] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [BitMascot] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [BitMascot] SET  MULTI_USER 
GO
ALTER DATABASE [BitMascot] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [BitMascot] SET DB_CHAINING OFF 
GO
ALTER DATABASE [BitMascot] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [BitMascot] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [BitMascot] SET DELAYED_DURABILITY = DISABLED 
GO
USE [BitMascot]
GO
/****** Object:  Table [dbo].[User]    Script Date: 4/23/2019 12:10:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[UserID] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [nvarchar](max) NULL,
	[LastName] [nvarchar](max) NULL,
	[Address] [nvarchar](max) NULL,
	[PhoneNo] [nvarchar](30) NULL,
	[Email] [nvarchar](max) NULL,
	[DOB] [datetime] NULL,
	[Password] [nvarchar](max) NULL,
	[UserType] [int] NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[User] ON 

INSERT [dbo].[User] ([UserID], [FirstName], [LastName], [Address], [PhoneNo], [Email], [DOB], [Password], [UserType]) VALUES (1, N'Arif', N'Rahman', N'New DOHS, Mohakhali, Dhaka - 1206.', N'01700000000', N'admin@localhost.local', CAST(N'2019-04-22 00:00:00.000' AS DateTime), N'$2a$12$IGvsfW2a9S0/eRSSk9X13eGa2sM5TYsY5RaJANnnprsfjMr54faQW', 1)
INSERT [dbo].[User] ([UserID], [FirstName], [LastName], [Address], [PhoneNo], [Email], [DOB], [Password], [UserType]) VALUES (2, N'Marylee ', N'G. Parker', N'3480 Peck Court El Toro, CA 92630', N'949-588-0735', N'abc1@xyz.com', CAST(N'2019-04-22 00:00:00.000' AS DateTime), N'$2a$12$6o6UYFHRFGrfngQoSilevuCWqayMsa2bD6VQrupHxIVTkSOZMm1wC', 1)
INSERT [dbo].[User] ([UserID], [FirstName], [LastName], [Address], [PhoneNo], [Email], [DOB], [Password], [UserType]) VALUES (3, N'Jackie ', N'F. Molina', N'4807 Everette Alley Fort Lauderdale, FL 33301', N'954-855-4695', N'abc2@xyz.com', CAST(N'1990-02-06 00:00:00.000' AS DateTime), N'$2a$12$wDVh.KkTCzKtNyZaHFv2Uemge6SpgEK8//HI98fCBFncojoUDLD8y', 2)
INSERT [dbo].[User] ([UserID], [FirstName], [LastName], [Address], [PhoneNo], [Email], [DOB], [Password], [UserType]) VALUES (4, N'', N'', N'', N'', N'admin@localhost.admin', CAST(N'2019-04-23 00:00:00.000' AS DateTime), N'$2a$12$2TzJskPiOIqMPMzbgou4lulgV19fCzfw70Z3JWeOGz0XgQFW9NjmW', 2)
SET IDENTITY_INSERT [dbo].[User] OFF
/****** Object:  StoredProcedure [dbo].[spUser_AddNewUser]    Script Date: 4/23/2019 12:10:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Nafees>
-- Create date: <22-4-19>
-- Description:	<Add new user>
-- =============================================
CREATE PROCEDURE [dbo].[spUser_AddNewUser]
	@firstName nvarchar(MAX) = ''
	, @lastName nvarchar(MAX) = ''
	, @address nvarchar(MAX) = ''
	, @phoneNo nvarchar(25) = ''
	, @email nvarchar(MAX)
	, @dob datetime = ''
	, @password nvarchar(MAX)
	, @userType nvarchar(MAX)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @isSuccessful int
	SET @isSuccessful = 0

	BEGIN TRY
		
		IF NOT EXISTS (SELECT [Email] FROM [User] WHERE [Email] = @email)
		BEGIN
			INSERT INTO [dbo].[User]([FirstName]
								, [LastName]
								, [Address]
								, [PhoneNo]
								, [Email]
								, [DOB]
								, [Password]
								, [UserType]) VALUES (@firstName
														, @lastName
														, @address
														, @phoneNo
														, @email
														, @dob
														, @password
														, @userType)
			SET @isSuccessful = 1
		END
		
		ELSE
		BEGIN
			SET @isSuccessful = 2
		END

	END TRY
	BEGIN CATCH
		SET @isSuccessful = 0
	END CATCH	

	SELECT @isSuccessful AS [Successful]
    

END

GO
/****** Object:  StoredProcedure [dbo].[spUser_GetUserByEmail]    Script Date: 4/23/2019 12:10:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Nafees>
-- Create date: <22-4-19>
-- Description:	<get User info by email>
-- =============================================
CREATE PROCEDURE [dbo].[spUser_GetUserByEmail]
	@email nvarchar(MAX)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT [UserID]
		, [FirstName]
		, [LastName]
		, [Address]
		, [PhoneNo]
		, [Email]
		, [DOB]
		, [Password]
		, [UserType]
	FROM [dbo].[User]
	WHERE [Email] = @email    

END

GO
/****** Object:  StoredProcedure [dbo].[spUser_GetUserByID]    Script Date: 4/23/2019 12:10:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Nafees>
-- Create date: <22-4-19>
-- Description:	<get User info by ID>
-- =============================================
CREATE PROCEDURE [dbo].[spUser_GetUserByID]
	@id int
AS
BEGIN
	SET NOCOUNT ON;

	SELECT [UserID]
		, [FirstName]
		, [LastName]
		, [Address]
		, [PhoneNo]
		, [Email]
		, [DOB]
		, [Password]
		, [UserType]
	FROM [dbo].[User]
	WHERE [UserID] = @id    

END

GO
/****** Object:  StoredProcedure [dbo].[spUser_GetUserList]    Script Date: 4/23/2019 12:10:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Nafees>
-- Create date: <22-4-19>
-- Description:	<get User list>
-- =============================================
CREATE PROCEDURE [dbo].[spUser_GetUserList]
AS
BEGIN
	SET NOCOUNT ON;

	SELECT [UserID]
		, [FirstName]
		, [LastName]
		, [Address]
		, [PhoneNo]
		, [Email]
		, [DOB]
		, [Password]
		, [UserType]
	FROM [dbo].[User]

END

GO
/****** Object:  StoredProcedure [dbo].[spUser_UpdateUserPassword]    Script Date: 4/23/2019 12:10:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Nafees>
-- Create date: <22-4-19>
-- Description:	<update password>
-- =============================================
CREATE PROCEDURE [dbo].[spUser_UpdateUserPassword]
	@id int
	, @password nvarchar(MAX)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @isSuccessful int
	SET @isSuccessful = 0

	BEGIN TRY
		
		UPDATE [dbo].[User]
		SET [Password] = @password
		WHERE [UserID] = @id
		
	SET @isSuccessful = 1

	END TRY
	BEGIN CATCH
		SET @isSuccessful = 0
	END CATCH  
	
	SELECT @isSuccessful AS [Successful]

END

GO
USE [master]
GO
ALTER DATABASE [BitMascot] SET  READ_WRITE 
GO
